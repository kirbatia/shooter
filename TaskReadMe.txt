The purpose of this test task is to make a simple shooter where the player stands still and rotates the camera, while shooting from enemy balls that spawn in a circle from the player and fly in his direction.
3 weapons must be implemented:
 * BFG (shoots physical, visible bullets. Has 3 bullets in the magazine);
 * Shotgun (shoots invisible raycasts instantly. Has 8 bullets in the magazine);
 * Dual Barrel Shotgun (same as Shotgun, but only has 2 bullets, both of which are expended during one shot).
Each gun has a limited amount of ammo and needs to be reloaded from time to time. While reloading, the player cannot shoot, but he can change weapons. If the weapon is changed, the reload is cancelled.
Each gun should have an adjustable cooldown between shots.
Weapons should be easily editable, for example, if the "designer" wants to change the BFG's fire type from physical to raycast,then he should be able to do it without getting into the code.
If an enemy reaches the player, their HP decreases and if HP drops to 0, the game ends and displays a losing screen where the player can start over (reload the level).
In the HUD, you need to display the player's HP and ammo counter.


The main thing that is evaluated is the modularity and scalability of the architecture. Making sounds/animations is optional, but will give bonus points :)

** With no external plugins such as UniRX, DOTween, Zenject etc.
