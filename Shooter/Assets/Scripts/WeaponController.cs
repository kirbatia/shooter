using System;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    [SerializeField] private WeaponType _type;
    [SerializeField] private Transform _fireSpawn;
    [SerializeField] private int _bulletCount = 2;
    [SerializeField] private float _rechargeTimeTotal = 2;
    [SerializeField] private float _coolDown = 1;
    [SerializeField] private LineRenderer _lineRenderer = null;
    private int _bulletsLeft;
    private bool _isActive;
    private float _rechargeTimeLeft;

    public WeaponType WeaponType => _type;
    public Transform FireSpawnPosition => _fireSpawn;
    public LineRenderer LaserRenderer => _lineRenderer;
    public event Action BulletCountChanged;
    public event Action<float> RechargeChanged;
    public event Action WeaponChanged;
    public event Action<bool> CoolDownSet;
    public int BulletsLeft
    {
        get => _bulletsLeft;
        set
        {
            _bulletsLeft = value;
            BulletCountChanged?.Invoke();
        }
    }
    
    public bool IsCoolDownActive
    {
        set => CoolDownSet?.Invoke(value);
    }

    public float RechargeTimeTotal => _rechargeTimeTotal;

    public float CoolDown => _coolDown;

    public float RechargeTimeLeft
    {
        get => _rechargeTimeLeft;
        set
        {
            _rechargeTimeLeft = value;
            RechargeChanged?.Invoke(_rechargeTimeLeft);
        }
    }

    public int CachedBulletCount => _bulletCount;

    public bool IsActive
    {
        get => _isActive;
        set
        {
            _isActive = value;
            WeaponChanged?.Invoke();
        }
    }

    public void Init()
    {
        _bulletsLeft = _bulletCount;
    }
}