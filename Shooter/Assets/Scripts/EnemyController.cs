using System;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
   private float _speed;
   private Vector3 _targetPosition;
   public event Action<Collider, EnemyController> Triggered;
   
   public EnemyController Init(float speed, Vector3 targetPosition)
   {
      _speed = speed;
      _targetPosition = targetPosition;
      return this;
   }
   private void FixedUpdate()
   {
      transform.position = Vector3.MoveTowards(transform.position, _targetPosition, _speed * Time.fixedDeltaTime);
   }

   private void OnTriggerEnter(Collider other)
   {
      Triggered?.Invoke(other, this);
   }
}