using UnityEngine;

public static class InputManager
{
#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBGL
    public static float GetAxis(string axisName)
    {
        return Input.GetAxis(axisName);
    }

    public static bool GetKey(KeyCode buttonKey)
    {
        return Input.GetKeyDown(buttonKey);
    }

#endif
}