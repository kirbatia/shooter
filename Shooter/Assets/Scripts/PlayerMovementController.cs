using UnityEngine;

public class PlayerMovementController : MonoBehaviour
{
    [SerializeField] private float _rotationSpeed = 30f;
    [SerializeField] private float _maxUpAngle = 40f;
    [SerializeField] private float _maxDownAngle = -30f;
    private void FixedUpdate()
    {
        float horizontalInput = InputManager.GetAxis("Horizontal");
        float verticalInput = InputManager.GetAxis("Vertical");

        if (Mathf.Approximately(horizontalInput, 0f) && Mathf.Approximately(verticalInput, 0f))
        {
            return;
        }

        float currentAngleX = Mathf.Clamp(transform.localEulerAngles.x, 0f, 360f);
        currentAngleX = currentAngleX > 180 ? currentAngleX - 360 : currentAngleX;
        float newAngleX = Mathf.Clamp(currentAngleX - verticalInput * _rotationSpeed * Time.fixedDeltaTime, _maxDownAngle, _maxUpAngle);
        float currentAngleY = transform.localEulerAngles.y;
        float newAngleY = currentAngleY + horizontalInput * _rotationSpeed * Time.fixedDeltaTime;
        transform.localRotation = Quaternion.Euler(newAngleX, newAngleY, transform.localEulerAngles.z);
    }
}