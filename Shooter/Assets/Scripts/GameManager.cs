using UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

public class GameManager : MonoBehaviour
{
    [SerializeField] private WeaponManager _weaponManager;
    [SerializeField] private EnemySpawnerController _enemySpawnerController;
    [SerializeField] private PlayerData _playerData;
    [SerializeField] private MainUiController _mainUiController;
    [FormerlySerializedAs("_effectsManager")] [SerializeField] private EffectController effectController;
    [SerializeField] private InputController _inputController;
    [SerializeField] private AudioManager _audioManager;
    private PlayerDataManager _playerDataManager;

    private void Awake()
    {
        _weaponManager.Init(_inputController);
        _audioManager.Init(_inputController,_weaponManager);
        _enemySpawnerController.Init(_weaponManager);
        _playerDataManager = new PlayerDataManager();
        _playerDataManager.Init(_playerData, _weaponManager, _enemySpawnerController);
        _mainUiController.Init(_playerDataManager, _weaponManager);
        effectController.Init(_weaponManager);
        _mainUiController.RestartClicked += OnRestartClicked;
    }

    private void OnRestartClicked()
    {
        _playerDataManager.UpdatePlayerData();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Time.timeScale = 1;
    }

    private void OnDestroy()
    {
        _mainUiController.RestartClicked -= OnRestartClicked;
        _playerDataManager.DeInit();
    }
}