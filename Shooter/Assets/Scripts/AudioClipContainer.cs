using UnityEngine;

[System.Serializable]
public class AudioClipContainer {
    public string Name;
    public AudioClip Clip;
}