using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioManager : MonoBehaviour
{
   private const string Bfg = "BFG";
   private const string ShotgunFire = "ShotgunFire";
   private const string BdShotgunFire = "BDShotgunFire";
   private const string ChangeWeapon = "ChangeWeapon";
   
   [SerializeField] private List<AudioClipContainer> clipNamesList;
   private AudioSource _audioSource;
   private InputController _inputController;
   private WeaponManager _weaponManager;
   private readonly Dictionary<string, AudioClip> _audioDictionary = new Dictionary<string, AudioClip>();

   private Dictionary<WeaponType, string> _audioWeaponDictionary = new Dictionary<WeaponType, string>()
   {
      { WeaponType.Bfg, Bfg },
      { WeaponType.Shotgun, ShotgunFire },
      { WeaponType.DBShotgun, BdShotgunFire }
   };

   public void Init(InputController inputController, WeaponManager weaponManager)
   {
      _audioSource = GetComponent<AudioSource>();
      _inputController = inputController;
      _weaponManager = weaponManager;
      _inputController.KeyPressed += OnKeyPressed;
      
      foreach (AudioClipContainer item in clipNamesList) {
         _audioDictionary[item.Name] = item.Clip;
      }
   }

   private void OnKeyPressed(KeyCode key)
   {
      switch (key)
      {
         case KeyCode.A:
         case KeyCode.D:
            Play(ChangeWeapon);
            break;
         case KeyCode.Space:
            if (_audioWeaponDictionary.TryGetValue(_weaponManager.ActiveWeaponType, out var clip)) 
            {
               Play(clip);
            } else
            {
               Debug.Log($"{clip} not found in dictionary");
            }
            break;
      }
   }

   private void Play(string audioClip)
   {
      if (_audioDictionary.TryGetValue(audioClip, out var clip)) 
      {
         _audioSource.PlayOneShot(clip);
      } else
      {
         Debug.Log($"{audioClip} not found in dictionary");
      }
   }

   private void OnDisable()
   {
      _inputController.KeyPressed -= OnKeyPressed;
   }
}