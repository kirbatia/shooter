using System;
using UnityEngine;

public class PlayerDataManager
{
    private PlayerData _playerData;
    private WeaponManager _weaponManager;
    private EnemySpawnerController _enemySpawner;

    public event Action<int> HealthChanged;
    public event Action<int> ScoreChanged;
    public event Action PlayerKilled;
    public PlayerData PlayerData => _playerData;
    public bool IsFirstPlayer => _playerData.IsFirstPlayer;

    public void Init(PlayerData data, WeaponManager weaponManager, EnemySpawnerController enemySpawnerController)
    {
        _playerData = data;
        _weaponManager = weaponManager;
        _enemySpawner = enemySpawnerController;
        _enemySpawner.PlayerHit += OnPlayerHit;
        _weaponManager.BulletTriggered += OnBulletTriggered;
    }

    public void UpdatePlayerData()
    {
        _playerData.Health = 50;
        _playerData.Score = 0;
    }

    public void UpdateFirstPlayerInfo(bool isFirstPlayer)
    {
        _playerData.IsFirstPlayer = isFirstPlayer;
    }

    public void DeInit()
    {
        _enemySpawner.PlayerHit -= OnPlayerHit;
        _weaponManager.BulletTriggered -= OnBulletTriggered;
    }
    
    private void OnPlayerHit()
    {
        _playerData.Health--;
        var hp = _playerData.Health;
        HealthChanged?.Invoke(hp);
        if (hp <= 0)
        {
            PlayerKilled?.Invoke();
        }
    }

    private void OnBulletTriggered(Collider collider)
    {
        var obj = collider.gameObject;
        if (obj.GetComponent<EnemyController>() == null)
        {
            return;
        }
        _playerData.Score++;
        ScoreChanged?.Invoke(_playerData.Score);
    }
}