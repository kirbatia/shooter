using System;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public event Action<KeyCode> KeyPressed;

    private void Update()
    {
        if (InputManager.GetKey(KeyCode.A))
        {
            KeyPressed?.Invoke(KeyCode.A);
        }
        if (InputManager.GetKey(KeyCode.D))
        {
            KeyPressed?.Invoke(KeyCode.D);
        }
        if (InputManager.GetKey(KeyCode.Space))
        {
            KeyPressed?.Invoke(KeyCode.Space);
        }
    }
}