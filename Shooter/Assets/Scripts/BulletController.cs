using System;
using UnityEngine;

public class BulletController : MonoBehaviour
{
   private const float Speed = 100;
   private Rigidbody _rb;
   
   public event Action<Collider, BulletController> Triggered;

   public void Shoot()
   {
      _rb.velocity = transform.position.normalized * Speed;
   }
   
   private void Awake() 
   {
      _rb = GetComponent<Rigidbody>();
   }
   
   private void OnTriggerEnter(Collider other)
   {
      Triggered?.Invoke(other, this);
   }
}