public struct EnemySpawnData
{
    public const float MaxDistance = 10;
    public const float MaxDistanceY = 3;
    public const float MinDistanceY = 0.5f;
    public const float SpawnDistanceLimit = 1;
    public const int PoolSize = 10;
    public const float SpeedAcceleration = 0.05f;
    public const float Delay = 3f;
    public const float StartRandom = 0;
}