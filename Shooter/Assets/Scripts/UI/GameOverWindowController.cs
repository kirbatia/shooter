using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
   public class GameOverWindowController : MonoBehaviour
   {
      [SerializeField] private Button _restartButton;

      public event Action RestartClicked;

      private void OnEnable()
      {
         _restartButton.onClick.AddListener(OnRestartClicked);
      }
   
      private void OnDisable()
      {
         _restartButton.onClick.RemoveListener(OnRestartClicked);
      }

      private void OnRestartClicked()
      {
         RestartClicked?.Invoke();
      }
   }
}
