using System.Collections.Generic;
using UnityEngine;

namespace UI
{
    public class WeaponWindowController : MonoBehaviour
    {
        [SerializeField] private List<WeaponIndicatorItem> _weaponIndicatorItems;
        private WeaponManager _weaponManager;
        
        public void Init(WeaponManager weaponManager)
        {
            _weaponManager = weaponManager;
            var list = _weaponManager.WeaponControllers;

            foreach (var item in _weaponIndicatorItems)
            {
                var x = list.Find(x => x.WeaponType == item.WeaponType);
                item.Init(x);
            }
        }
    }
}