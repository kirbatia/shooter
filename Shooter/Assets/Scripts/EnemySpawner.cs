using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemySpawner
{
    private List<EnemyController> _enemiesList = new List<EnemyController>();
    private ObjectPool _pool;
    private EnemyController _enemyController;
    private WeaponManager _weaponManager;

    public event Action PlayerHit;

    public EnemySpawner(EnemyController enemyController, WeaponManager weaponManager)
    {
        _weaponManager = weaponManager;
        _enemyController = enemyController;
    }

    public void Init()
    {
        _pool = new ObjectPool(_enemyController.gameObject, EnemySpawnData.PoolSize);
        _weaponManager.BulletTriggered += ReturnToPool;
    }

    public IEnumerator Spawn(float speed)
    {
        while (true)
        {
            var randomX = GetRandom(EnemySpawnData.StartRandom);
            var randomZ = GetRandom(EnemySpawnData.StartRandom);
            var randomY = Random.Range(EnemySpawnData.MinDistanceY, EnemySpawnData.MaxDistanceY);
            
            var enemy = _pool.GetObject<EnemyController>();
            enemy.transform.position = new Vector3(randomX, randomY, randomZ);
            enemy.transform.LookAt(Vector3.zero);
            enemy.Init(speed, Vector3.zero);
            enemy.Triggered += OnHit;
            _enemiesList.Add(enemy);
            speed += EnemySpawnData.SpeedAcceleration;
            
            yield return new WaitForSeconds(EnemySpawnData.Delay);
        }
    }

    private float GetRandom(float point)
    {
        while (point is >= -EnemySpawnData.SpawnDistanceLimit and <= EnemySpawnData.SpawnDistanceLimit)
        {
            point = Random.Range(-EnemySpawnData.MaxDistance, EnemySpawnData.MaxDistance);
        }

        return point;
    }

    private void ReturnToPool(Collider collider)
    {
        var obj = collider.gameObject;
        if (obj.GetComponent<EnemyController>() == null)
        {
            return;
        }
        _pool.ReturnObject(obj);
    }
    
    private void OnHit(Collider collider, EnemyController controller)
    {
        if (collider.gameObject.GetComponent<PlayerMovementController>() != null)
        {
            PlayerHit?.Invoke();
        }
       
        controller.Triggered -= OnHit;
        _enemiesList.Remove(controller);
        _pool.ReturnObject(controller.gameObject);
    }

    public void Destroy()
    {
        foreach (var enemy in _enemiesList)
        {
            enemy.Triggered -= OnHit;
        }

        _enemiesList.Clear();
        _weaponManager.BulletTriggered -= ReturnToPool;
    }
}