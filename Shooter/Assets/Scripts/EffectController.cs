using System.Collections;
using UnityEngine;

public class EffectController : MonoBehaviour
{
   [SerializeField] private ParticleSystem _explosion;
   private ObjectPool _pool;
   private WeaponManager _weaponManager;

   public void Init(WeaponManager weaponManager)
   {
      _weaponManager = weaponManager;
      _pool = new ObjectPool(_explosion.gameObject, 5);
      _weaponManager.BulletTriggered += PlayExplosion;
   }

   private void PlayExplosion(Collider obj)
   {
      var explosion = _pool.GetObject<Transform>();
      explosion.transform.position = obj.transform.position;
      StartCoroutine(ReturnAfterDelay(explosion.gameObject, 1.5f));
   }
   
   private IEnumerator ReturnAfterDelay(GameObject obj, float time)
   {
      yield return new WaitForSeconds(time);
      _pool.ReturnObject(obj);
   }
}
